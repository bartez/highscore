﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Highscore.Models;

namespace Highscore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HighscoresController : ControllerBase
    {
        private readonly HighscoreContext _context;

        public HighscoresController(HighscoreContext context)
        {
            _context = context;
        }

        // GET: api/Highscores
        [HttpGet]
        public async Task<ActionResult<IEnumerable<HighscoreItemEntity>>> GetHighscoreItemEntity()
        {
            return await _context.HighscoreItemEntity.ToListAsync();
        }

        // GET: api/Highscores/5
        [HttpGet("{id}")]
        public async Task<ActionResult<HighscoreItemEntity>> GetHighscoreItemEntity(int id)
        {
            var highscoreItemEntity = await _context.HighscoreItemEntity.FindAsync(id);

            if (highscoreItemEntity == null)
            {
                return NotFound();
            }

            return highscoreItemEntity;
        }

        // PUT: api/Highscores/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutHighscoreItemEntity(int id, HighscoreItemEntity highscoreItemEntity)
        {
            if (id != highscoreItemEntity.Id)
            {
                return BadRequest();
            }

            _context.Entry(highscoreItemEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HighscoreItemEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Highscores
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<HighscoreItemEntity>> PostHighscoreItemEntity(HighscoreItemEntity highscoreItemEntity)
        {
            _context.HighscoreItemEntity.Add(highscoreItemEntity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetHighscoreItemEntity", new { id = highscoreItemEntity.Id }, highscoreItemEntity);
        }

        // DELETE: api/Highscores/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<HighscoreItemEntity>> DeleteHighscoreItemEntity(int id)
        {
            var highscoreItemEntity = await _context.HighscoreItemEntity.FindAsync(id);
            if (highscoreItemEntity == null)
            {
                return NotFound();
            }

            _context.HighscoreItemEntity.Remove(highscoreItemEntity);
            await _context.SaveChangesAsync();

            return highscoreItemEntity;
        }

        private bool HighscoreItemEntityExists(int id)
        {
            return _context.HighscoreItemEntity.Any(e => e.Id == id);
        }
    }
}

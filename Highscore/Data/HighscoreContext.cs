﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Highscore.Models
{
    public class HighscoreContext : DbContext
    {
        public HighscoreContext (DbContextOptions<HighscoreContext> options)
            : base(options)
        {
        }

        public DbSet<Highscore.Models.HighscoreItemEntity> HighscoreItemEntity { get; set; }
    }
}

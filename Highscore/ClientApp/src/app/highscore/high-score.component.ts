import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-high-score',
  templateUrl: './high-score.component.html'
})
export class HighScoreComponent {
  public highScores: HighScore[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<HighScore[]>(baseUrl + 'api/Highscores').subscribe(result => {
      this.highScores = result.sort((a, b) => (a.score > b.score) ? -1 : 1);

    }, error => console.error(error));
  }
}

interface HighScore {
  name: string;
  score: number;
  date: string;
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Highscore.Models
{
    public class HighscoreItemEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public long Score { get; set; }
        public DateTime Date { get; set; }
    }
}
